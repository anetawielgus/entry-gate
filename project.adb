--PATH="/home/student/Desktop/bin:$PATH"; export PATH
-- panel.adb
--
-- materiały dydaktyczne
-- 2016
-- (c) Jacek Piwowarczyk
--

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Float_Text_IO;
use Ada.Float_Text_IO;

with Ada.Calendar;
use Ada.Calendar;

with Ada.Text_IO, Ada.Calendar, Ada.Calendar.Formatting;
use Ada.Text_IO, Ada.Calendar, Ada.Calendar.Formatting;

with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;

with Ada.Exceptions;
use Ada.Exceptions;

procedure Project is

  Koniec : Boolean := False with Atomic;

  type Atrybuty is (Czysty, Jasny, Podkreslony, Negatyw, Migajacy, Szary);

  SygnalPilota                : Boolean := False;
  CzujnikOtwarcia             : Boolean := False;
  CzujnikZamkniecia           : Boolean := False;
  CzujnikPrzeszkody           : Boolean := False;
  CzujnikPrzeszkodyPrzekazany : Boolean := False;
  Sekunda                     : constant Duration := 1.0;
  SavedClock                  : Time;
  PozycjaBramy : Integer := 1;

  type Stany is (ZAMKNIETA, OTWIERANIE, OTWARTA, ZAMYKANIE, PRZESZKODA);
  AktualnyStan : Stany := ZAMKNIETA with Atomic;

  task Pilot;
  task body Pilot is
    KlawiszPilot : Character;
    Available : Boolean;
    SavedClockPilot : Time;
  begin
     loop
        SavedClockPilot := Clock;
        delay until SavedClockPilot + 0.5;
       Get_Immediate(KlawiszPilot, Available);
       if (Available = True) then
          if KlawiszPilot in 'r'|'R' then                
              SygnalPilota := True;
           else
              SygnalPilota := False;
           end if;
           if KlawiszPilot in 'o' then                    
              CzujnikPrzeszkody := True;
             delay until SavedClockPilot + 1.0;    
             CzujnikPrzeszkody := False;       
             elsif KlawiszPilot in 'O' then                 
             CzujnikPrzeszkody := True;
             delay until SavedClockPilot + 5.0;
             CzujnikPrzeszkody := False;
           else
              CzujnikPrzeszkody := False;
           end if;
        end if;
        exit when KlawiszPilot in 'q'|'Q';            
     end loop;
     Koniec := True;
  end Pilot;

  task CzujnikOtwarciaSygnal;
  task body CzujnikOtwarciaSygnal is
  begin
     loop
        if PozycjaBramy = 31 then
           CzujnikOtwarcia := True;
        else
           CzujnikOtwarcia := False;
        end if;
        exit when Koniec;
     end loop;
  end CzujnikOtwarciaSygnal;

  task CzujnikZamknieciaSygnal;
  task body CzujnikZamknieciaSygnal is
  begin
     loop
        if PozycjaBramy = 1 then
           CzujnikZamkniecia := True;
        else
           CzujnikZamkniecia := False;
        end if;
        exit when Koniec;
     end loop;
  end CzujnikZamknieciaSygnal;

 task Brama;
  task body Brama is
     SavedClockBrama : Time;
  begin
     loop
        SavedClockBrama := Clock;
        if AktualnyStan = ZAMKNIETA then
          PozycjaBramy := 1;
        elsif AktualnyStan = OTWIERANIE then
           if (CzujnikOtwarcia = False) and AktualnyStan = OTWIERANIE then
                PozycjaBramy := PozycjaBramy + 1;
              exit when Koniec;
           end if;
        elsif AktualnyStan = OTWARTA then
        PozycjaBramy := PozycjaBramy;
        elsif AktualnyStan = ZAMYKANIE then
           CzujnikPrzeszkodyPrzekazany := False;
           if (CzujnikZamkniecia = False) and AktualnyStan = ZAMYKANIE then
                PozycjaBramy := PozycjaBramy - 1;
              exit when Koniec;
           end if;
        elsif AktualnyStan = PRZESZKODA then
          PozycjaBramy := PozycjaBramy;
        end if;
        exit when Koniec;
        delay until SavedClockBrama + 0.2;
     end loop;
  end Brama;

  protected Ekran  is
    procedure Pisz_XY(X,Y: Positive; S: String; Atryb : Atrybuty := Czysty);
    procedure Pisz_Float_XY(X, Y: Positive;
                            Num: Float;
                            Pre: Natural := 3;
                            Aft: Natural := 2;
                            Exp: Natural := 0;
                            Atryb : Atrybuty := Czysty);
    procedure Czysc;
    procedure Tlo;
  end Ekran;

  protected body Ekran is
    -- implementacja dla Linuxa i macOSX
    function Atryb_Fun(Atryb : Atrybuty) return String is
      (case Atryb is
       when Jasny => "1m", when Podkreslony => "4m", when Negatyw => "7m",
       when Migajacy => "5m", when Szary => "2m", when Czysty => "0m");

    function Esc_XY(X,Y : Positive) return String is
      ( (ASCII.ESC & "[" & Trim(Y'Img,Both) & ";" & Trim(X'Img,Both) & "H") );

    procedure Pisz_XY(X,Y: Positive; S: String; Atryb : Atrybuty := Czysty) is
      Przed : String := ASCII.ESC & "[" & Atryb_Fun(Atryb);
    begin
      Put( Przed);
      Put( Esc_XY(X,Y) & S);
      Put( ASCII.ESC & "[0m");
    end Pisz_XY;

    procedure Pisz_Float_XY(X, Y: Positive;
                            Num: Float;
                            Pre: Natural := 3;
                            Aft: Natural := 2;
                            Exp: Natural := 0;
                            Atryb : Atrybuty := Czysty) is

      Przed_Str : String := ASCII.ESC & "[" & Atryb_Fun(Atryb);
    begin
      Put( Przed_Str);
      Put( Esc_XY(X, Y) );
      Put( Num, Pre, Aft, Exp);
      Put( ASCII.ESC & "[0m");
    end Pisz_Float_XY;

    procedure Czysc is
    begin
      Put(ASCII.ESC & "[2J");
    end Czysc;

    procedure Tlo is
    begin
      Ekran.Czysc;
      Ekran.Pisz_XY(1,1,"+=========== Entry Gate ===========+");
      Ekran.Pisz_XY(3,4,"Gate State =");
      Ekran.Pisz_XY(3,5,"Open Sensor =");
      Ekran.Pisz_XY(3,6,"Close Sensor =");
      Ekran.Pisz_XY(3,7,"Obstacle =");
      Ekran.Pisz_XY(1,10,"+=== Q-quit, R-run, o|O-obstacle ===+");
    end Tlo;

  end Ekran;

  task Przebieg;

  task body Przebieg is

    Nastepny     : Ada.Calendar.Time;
    Okres        : constant Duration := 0.1; -- sekundy
    Przesuniecie : constant Duration := 0.1;

  begin
    Nastepny := Clock + Przesuniecie;
    loop
      delay until Nastepny;
      Ekran.Pisz_XY(16 ,4,10*' ',Atryb=>Czysty);
      Ekran.Pisz_XY(16 ,4,AktualnyStan'Img,Atryb=>Podkreslony);
      Ekran.Pisz_XY(17 ,5,5*' ',Atryb=>Czysty);
      Ekran.Pisz_XY(17 ,5,CzujnikOtwarcia'Img,Atryb=>Podkreslony);
      Ekran.Pisz_XY(18 ,6,5*' ',Atryb=>Czysty);
      Ekran.Pisz_XY(18 ,6, CzujnikZamkniecia'Img,Atryb=>Podkreslony);
      Ekran.Pisz_XY(14 ,7,5*' ',Atryb=>Czysty);
      Ekran.Pisz_XY(14 ,7, CzujnikPrzeszkody'Img,Atryb=>Podkreslony);

      Ekran.Pisz_XY(42, 4,30*" ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 5,30*" ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 6,30*" ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 7,30*" ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 8,30*" ",Atryb=>Negatyw);

      Ekran.Pisz_XY(42,4,PozycjaBramy*" ",Atryb=>Czysty);
      Ekran.Pisz_XY(42,5,PozycjaBramy*" ",Atryb=>Czysty);
      Ekran.Pisz_XY(42,6,PozycjaBramy*" ",Atryb=>Czysty);
      Ekran.Pisz_XY(42,7,PozycjaBramy*" ",Atryb=>Czysty);
      Ekran.Pisz_XY(42,8,PozycjaBramy*" ",Atryb=>Czysty);
      Ekran.Pisz_XY(40 + PozycjaBramy+1 ,4," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(40 + PozycjaBramy+1 ,5," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(40 + PozycjaBramy+1 ,6," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(40 + PozycjaBramy+1 ,7," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(40 + PozycjaBramy+1 ,8," ",Atryb=>Negatyw);

      Ekran.Pisz_XY(42, 4," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 5," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 6," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 7," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(42, 8," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(72,4," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(72,5," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(72,6," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(72,7," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(72,8," ",Atryb=>Negatyw);
      Ekran.Pisz_XY(1 ,11,"",Atryb=>Czysty);
      exit when Koniec;
      Nastepny := Nastepny + Okres;
    end loop;
    Ekran.Pisz_XY(1,11,"");
    exception
      when E:others =>
        Put_Line("Error: Zadanie Przebieg");
        Put_Line(Exception_Name (E) & ": " & Exception_Message (E));
  end Przebieg;
AktywnaBrama : Boolean := False;
Zn: Character := ' '; 
Pl: File_Type;
Nazwa: String := "dziennik.log";
Czas: Time;
SavedClockZamknijPoOtwarciu : Time;

begin
	Create(Pl, Append_File, Nazwa);
  Czas := Clock;
  Put_Line(Pl, "gate started-up " & Image(Czas));
  Put_Line(Pl, "gate closed " & Image(Czas));
  -- inicjowanie
  Ekran.Tlo;
    loop
      SavedClock := Clock;
    if (SygnalPilota = True) then
      AktywnaBrama := True;
      SygnalPilota := False;
    end if;

      if AktualnyStan = ZAMKNIETA then
         if (AktywnaBrama = True) then
            AktualnyStan := OTWIERANIE;
            Czas := Clock;
            Put_Line(Pl, "gate opening " & Image(Czas));
         else
            AktualnyStan := ZAMKNIETA;
         end if;
      elsif AktualnyStan = OTWIERANIE then
         if (CzujnikOtwarcia = True) then
            AktualnyStan := OTWARTA;
            AktywnaBrama := False;
            Czas := Clock;
            Put_Line(Pl, "gate opened " & Image(Czas));
            SavedClockZamknijPoOtwarciu := Clock;
         else
            AktualnyStan := OTWIERANIE;
            AktywnaBrama := True;
         end if;
      elsif AktualnyStan = OTWARTA then
        if (CzujnikPrzeszkodyPrzekazany = True) then
            AktualnyStan := ZAMYKANIE;
            AktywnaBrama := True;
            Czas := Clock;
            Put_Line(Pl, "gate closing " & Image(Czas)); 
         elsif (CzujnikPrzeszkodyPrzekazany = False) then
            if (AktywnaBrama = True) then
               AktualnyStan := ZAMYKANIE;
               AktywnaBrama := True;
               Czas := Clock;
              Put_Line(Pl, "gate closing " & Image(Czas));
            elsif SavedClockZamknijPoOtwarciu + 10.0 < Clock then
              AktualnyStan := ZAMYKANIE;
              AktywnaBrama := True;
              Czas := Clock;
              Put_Line(Pl, "gate closing " & Image(Czas));
            else
              AktualnyStan := OTWARTA;
              AktywnaBrama := False;
            end if;
        else
           AktualnyStan := OTWARTA;
           AktywnaBrama := False;
        end if;
      elsif AktualnyStan = ZAMYKANIE then
         if (CzujnikZamkniecia = True) then
            AktualnyStan := ZAMKNIETA;
            AktywnaBrama := False;
            Czas := Clock;
            Put_Line(Pl, "gate closed " & Image(Czas));
         elsif (CzujnikZamkniecia = False) then
            if (CzujnikPrzeszkody = True) then
               AktualnyStan := PRZESZKODA;
               AktywnaBrama := False;
               Czas := Clock;
               Put_Line(Pl, "obstacle detected " & Image(Czas));
            else
               AktualnyStan := ZAMYKANIE;
               AktywnaBrama := True;
            end if;
         else
            AktualnyStan := ZAMYKANIE;
         end if;
      elsif AktualnyStan = PRZESZKODA then
         while Clock < (SavedClock + 3.0) loop --przez 3 sekundy jest w tej petli i sprawdza czujnikPrzeszkody. jak w trakcie 3 sekund sie czujni zmieni na 0 to wychodzi z petli i ponownie sie ZAMYKANIE, a jak nie to po sekundzie wchodzi w OTWIERANIE
            if (CzujnikPrzeszkody = False) then
               CzujnikPrzeszkodyPrzekazany := False;
               AktualnyStan := ZAMYKANIE;
               AktywnaBrama := True;
               exit;
            else
               CzujnikPrzeszkodyPrzekazany := True;
               AktualnyStan := PRZESZKODA;
               AktywnaBrama := False;
            end if;
            exit when Koniec;
         end loop;
         if CzujnikPrzeszkodyPrzekazany = True then
            AktualnyStan := OTWIERANIE;
            AktywnaBrama := True;
            Czas := Clock;
            Put_Line(Pl, "gate opening " & Image(Czas));
         else
            AktualnyStan := ZAMYKANIE;
            AktywnaBrama := True;
            Czas := Clock;
            Put_Line(Pl, "gate closing " & Image(Czas));
           end if;
      end if;

      exit when Koniec;
   end loop;
  Czas := Clock;
  Put_Line(Pl, "gate off " & Image(Czas));
end Project;
